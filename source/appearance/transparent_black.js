class Appearance {
  fill () {
    return {
      r: 0.0,
      g: 0.0,
      b: 0.0,
      a: 0.8
    };
  }

  stroke () {
    return false;
  }
}
