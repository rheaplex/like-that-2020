class Appearance {
  fill () {
    //return 'rgb(250, 230, 210)'; // ... a , 200)';
    return {
      r: 0.98,
      g: 0.90,
      b: 0.82,
      a: 1.0
    };
  }

  stroke () {
    return false;
  }
}
