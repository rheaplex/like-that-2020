class Appearance {
  fill () {
    return {
      r: 0.0,
      g: 1.0,
      b: 0.0,
      a: 1.0
    };
  }

  stroke () {
    return false;
  }
}
